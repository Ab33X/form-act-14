<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cuenta;

class CuentaController extends Controller
{
    public function saveCuenta(Request $request)
    {
        $maxNumber = Cuenta::max("numero_cuenta");
        $cuenta = new Cuenta();
        $cuenta->numero_cuenta = $maxNumber + 1;
        $cuenta->nombre = $request->nombre;
        $cuenta->apellidos = $request->apellidos;
        $cuenta->edad = $request->edad;
        $cuenta->pais = $request->pais;
        $cuenta->ciudad = $request->ciudad;
        $cuenta->direccion = $request->direccion;
        $cuenta->balance = $request->balance;
        if ($cuenta->save()) {
            $data = array(
                "msg" => "Cuenta saved",
                "cuenta" => $cuenta,
                "cuentas" => Cuenta::all()
            );
        } else {
            $data = array(
                "msg" => "Cuenta not saved",
                "cuenta" => $cuenta
            );
        }

        return view('form');
    }
}
