<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\test;

class testcontroller extends Controller
{
    public function saveTest(Request $request){
        $test = new test;
        $test->field1 = $request->field1;
        $test->field2 = $request->field2;
        $test->field3 = $request->field3;
        $test->field4 = $request->field4;
        if($test->save()){
            $data = array(
                "msg" => "Test saved",
                "test" => $test
            );
        } else {
            $data = array(
                "msg" => "Test not saved",
                "test" => $test
            );
        }
        return response()->json($data);
    }

    public function getTest(){
        $data = test::all();
        return response()->json($data);
    }
}
