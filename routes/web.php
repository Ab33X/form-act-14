<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\testcontroller;
use App\Models\Cuenta;
use App\Http\Controllers\CuentaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/test/get', [testcontroller::class, 'getTest']);

Route::post('/test/save', [testcontroller::class, 'saveTest']);

Route::get('/form', function(){    
    return view('form');
});

Route::post('/form', [CuentaController::class, 'saveCuenta']);

Route::get('/cuentas', function(){    
    $cuentas = Cuenta::all();
    return view('cuentas', ["cuentas" => $cuentas]);
});


