<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->id("id_cuenta");
            $table->string("numero_cuenta");
            $table->string("nombre");
            $table->string("apellidos");
            $table->integer("edad");
            $table->string("pais");
            $table->string("ciudad");
            $table->string("direccion");
            $table->double("balance", 12, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
    }
}
