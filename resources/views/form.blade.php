<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulario Act 14</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container pt-5">
        <div class="row">
            <div class="col">
                <h2>Formulario de cuenta</h2>
            </div>
            <div class="col">
                <a class="btn btn-dark ml-auto" href="/" role="button">Regresar</a>
            </div>
          </div>
        <div class="w-50">
            <form method="POST" action="/form">
                {{ csrf_field() }}
                <div class="mb-3">
                    <label class="form-label">Nombre</label>
                    <input required type="text" name="nombre" class="form-control" maxlength="30">
                </div>
                <div class="mb-3">
                    <label class="form-label">Apellidos</label>
                    <input required type="text" name="apellidos" class="form-control" maxlength="30">
                </div>
                <div class="mb-3 form-check">
                    <label class="form-label">Edad</label>
                    <input required type="number" name="edad" step="1" pattern="\d+" class="form-control" maxlength="30">
                </div>
                <div class="mb-3">
                    <label class="form-label">Pais</label>
                    <input required type="text" name="pais" class="form-control" maxlength="30">
                </div>
                <div class="mb-3">
                    <label class="form-label">Ciudad</label>
                    <input required type="text" name="ciudad" class="form-control" maxlength="30">
                </div>
                <div class="mb-3">
                    <label class="form-label">Direccion</label>
                    <input required type="text" name="direccion" class="form-control" maxlength="30">
                </div>
                <div class="mb-3 form-check">
                    <label class="form-label">Balance</label>
                    <input required type="number" name="balance" class="form-control" min="0" max="500" step="any" maxlength="30">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

</body>

</html>