## About Activity 14

We needed to create a form that stores accounts information in a database, for testing purposes, so we use Laravel famework to build a simple page. We use PostgreSQL 9.6 as database and Bootstrap for the front end. It is simple, but functional.

You need to configure your database if you wanna try this project, and edit the ".env" file in the root folder, in order to connect and migrate the necessary tables for the project.

## Routes

Our project has 3 main routes:

- "/" - It is the welcome page, it let us navigate to the capture form or the registered accounts view.
- "/form" - This is the main form, it receives accounts data and storage it in the database.
- "/cuentas" - This is the accounts view, it displays all accounts registered in the database.

## Coders and contributors

- Abraham Tenorio
- Carlos Corzo
- Adrian Treviño

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
